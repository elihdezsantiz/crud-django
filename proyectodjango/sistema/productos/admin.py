from django.contrib import admin
from .models import Producto, Proveedores
# Register your models here.
admin.site.register(Producto)
admin.site.register(Proveedores)