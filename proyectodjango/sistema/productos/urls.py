from django.urls import path
from . import views
 


urlpatterns = [
    
    path('',views.home, name="home"),
    path('registrarmedicamento/',views.registrarmedicamento),
    path('edicionmedicamento/<codigo>', views.edicionmedicamento),
    path('editarmedicamento/', views.editarmedicamento),
    path('eliminarmedicamento/<codigo>',views.eliminarmedicamento),
    path('proveedores/',views.homeproveedor, name="proveedores"),
    path('registrarproveedor/',views.registrarproveedor),
    path('proveedores/edicionproveedor/<identificador>', views.edicionproveedor),
    path('editarproveedor/', views.editarproveedor),
    path('proveedores/eliminarproveedor/<identificador>',views.eliminarproveedor)
]