from django.db import models

# Create your models here.
class Producto(models.Model):
    codigo = models.CharField(primary_key=True, max_length=3)
    nombre = models.CharField(max_length=58)
    precio = models.PositiveSmallIntegerField()

    def __str__(self):
        texto="{0} ({1}))"
        return texto.format(self.nombre, self.precio)


class Proveedores(models.Model):
    identificador = models.CharField(primary_key = True, max_length=3)
    nombres = models.CharField(max_length = 100)
    apellidos = models.CharField(max_length = 100)
    nacionalidad = models.CharField(max_length = 100)
    telefono = models.IntegerField()

    def __str__(self):
        textos="{0} ({1})"
        return textos.format(self.nombres, self.telefono)



