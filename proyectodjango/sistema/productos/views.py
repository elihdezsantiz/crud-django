##from django.forms import modelform_factory
from django.shortcuts import render, redirect
from .models import Producto, Proveedores
from django.db.models import Q
from django.contrib import messages
from django.contrib.auth.decorators import login_required
# Create your views here.
@login_required
def home(request):
    busqueda = request.GET.get("buscar")
    medicamento = Producto.objects.all()

    if busqueda:
        medicamento = Producto.objects.filter(
            Q(codigo__icontains = busqueda) |
            Q(nombre__icontains = busqueda) |
            Q(precio__icontains = busqueda)
        ).distinct()


    return render(request, "gestionProductos.html",{"medicamentos":medicamento})


def registrarmedicamento(request):
    codigo = request.POST['txtCodigo']
    nombre = request.POST['txtNombre']
    precio = request.POST['txtPrecio']
    
    medicamento =  Producto.objects.create(codigo=codigo, nombre=nombre, precio=precio)
    messages.success(request, '¡Medicamento Nuevo Agregado!')
    return redirect('/')

def edicionmedicamento(request, codigo):
    medicamento = Producto.objects.get(codigo=codigo)
    return render(request, "edicionmedicamento.html", {"medicamento":medicamento })


def editarmedicamento(request):
    codigo = request.POST['txtCodigo']
    nombre = request.POST['txtNombre']
    precio = request.POST['txtPrecio']

    medicamento= Producto.objects.get(codigo=codigo)

    medicamento.codigo = codigo
    medicamento.nombre = nombre
    medicamento.precio = precio
    medicamento.save()
    messages.success(request, '¡Medicamento Actualizado!')
    return redirect('/')


def eliminarmedicamento(request, codigo):
    medicamento= Producto.objects.get(codigo=codigo)
    medicamento.delete()
    messages.success(request, '¡Medicamento Eliminado!')
    return redirect('/')


def homeproveedor(request):
    busquedap = request.GET.get("buscador")
    proveedor = Proveedores.objects.all()

    if busquedap:
        proveedor = Proveedores.objects.filter(
            Q(identificador__icontains = busquedap) |
            Q(nombres__icontains = busquedap) |
            Q(apellidos__icontains = busquedap)|
            Q(nacionalidad__icontains = busquedap)|
            Q(telefono__icontains = busquedap)
        ).distinct()


    return render(request, "proveedores.html",{"proveedores":proveedor})



def registrarproveedor(request):
    identificador = request.POST['txtId']
    nombres = request.POST['txtNombres']
    apellidos = request.POST['txtApellidos']
    nacionalidad = request.POST['txtNacionalidad']
    telefono = request.POST['txtTelefono']

    
    proveedor =  Proveedores.objects.create(identificador=identificador, nombres=nombres, apellidos=apellidos, nacionalidad=nacionalidad,telefono=telefono)
    messages.success(request, '¡Proveedor Nuevo Agregado!')
    return redirect('/proveedores')

def edicionproveedor(request, identificador):
    proveedor = Proveedores.objects.get(identificador=identificador)
    return render(request, "edicionproveedor.html", {"proveedor":proveedor })


def editarproveedor(request):
    identificador = request.POST['txtId']
    nombres = request.POST['txtNombres']
    apellidos = request.POST['txtApellidos']
    nacionalidad = request.POST['txtNacionalidad']
    telefono = request.POST['txtTelefono']


    proveedor = Proveedores.objects.get(identificador=identificador)

    proveedor.identificador = identificador
    proveedor.nombres = nombres
    proveedor.apellidos = apellidos
    proveedor.nacionalidad = nacionalidad
    proveedor.telefono = telefono
    proveedor.save()
    messages.success(request, '¡Proveedor Actualizado!')
    return redirect('/proveedores')


def eliminarproveedor(request, identificador):
    proveedor= Proveedores.objects.get(identificador=identificador)
    proveedor.delete()
    messages.success(request, '¡Proveedor Eliminado!')
    return redirect('/proveedores')
