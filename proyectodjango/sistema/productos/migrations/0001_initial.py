# Generated by Django 3.2.9 on 2021-11-28 18:03

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Producto',
            fields=[
                ('codigo', models.CharField(max_length=3, primary_key=True, serialize=False)),
                ('nombre', models.CharField(max_length=58)),
                ('Precio', models.PositiveSmallIntegerField()),
            ],
        ),
    ]
